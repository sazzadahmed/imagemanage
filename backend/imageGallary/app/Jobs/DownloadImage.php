<?php

namespace App\Jobs;

use App\Models\Image;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Intervention\Image\ImageManagerStatic as ImageI;

class DownloadImage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $requestData;
    private $user;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($requestData,$user)
    {
        $this->requestData = $requestData;
        $this->user = $user;
        Log::info('Data get from request');
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $path = $this->requestData['path'];
        $filename = basename($path);
        ImageI::make($path)->save(public_path('images/'.count($this->user->images). $filename));
        $image = new Image();
        $image->title = $filename;
        $image->path = 'http://'.request()->getHttpHost().'/images/' .count($this->user->images). $filename;
        $this->user->images()->saveMany([$image]);
        event(new \App\Events\ImageSaveEvent('Event calling after image saving',$image));
    }
}
