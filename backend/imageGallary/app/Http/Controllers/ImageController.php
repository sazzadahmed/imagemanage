<?php

namespace App\Http\Controllers;

use App\Jobs\DownloadImage;
use App\Models\Image;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Log\Logger;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic  as ImageI;

class ImageController extends Controller
{
    public function getAll(Request $request) {
        $user = Auth::user();
        return $user->images;
    }



    public function new(Request $request, Logger $logger) {

        $requestData = $request->json()->all();
        $user = Auth::user();
        $jobs = (new DownloadImage($requestData, $user))->delay(Carbon::now()->addSecond(10));
        dispatch($jobs);
        return new JsonResponse(['ok'],200);

    }
}
