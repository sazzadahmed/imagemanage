<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{

    public function login(Request  $request)
    {

        $loginData =  $request->validate([
            "email" => "email|required",
            "password" => "required|max:100"
        ]);

        if (!auth()->attempt($loginData)) {
            return response(["msg" => "invalid user"] , 401);
        }
        $accessToken = auth()->user()->createToken('authToken')->accessToken;
        return response(['user' => auth()->user(), 'accessToken' => $accessToken]);


    }

    public function register(Request $request)
    {

        $validatedData = $request->validate([
            "name" => "required|max:55",
            "email" => "email|required",
            "password" => "required|max:100"
        ]);
        $validatedData["password"] = bcrypt($validatedData["password"]);
        $User = User::create($validatedData);
        $accessToken = $User->createToken('authToken')->accessToken;

        return response(['user' => $User, 'accessToken' => $accessToken]);
    }


}
