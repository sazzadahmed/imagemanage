<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::group([
    'middleware'=>'auth:api'
], function ()
{
    Route::get('/images',[\App\Http\Controllers\ImageController::class, 'getAll']);
    Route::post('/images',[\App\Http\Controllers\ImageController::class, 'new']);
});

Route::post('/register',[\App\Http\Controllers\AuthController::class, 'register']);
Route::post('/login',[\App\Http\Controllers\AuthController::class, 'login']);
