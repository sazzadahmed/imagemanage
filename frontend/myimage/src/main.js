import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import router from './router'
import Echo from 'laravel-echo';

Vue.config.productionTip = false


window.Pusher = require('pusher-js');
window.Echo = new Echo({
    broadcaster: 'pusher',
    key: '6009c5a34bfa02c31cda',
    cluster : "ap1",
    encripted: false
});

window.Echo.channel('my-channel')
    .listen('ImageSaveEvent', (e) => {
        console.log(e);
    });

new Vue({
  vuetify,
  router,
  render: h => h(App)
}).$mount('#app')
